<?php


?>

<h1><?php echo $titulo; ?> Chamados</h1>

<form method="post" action="<?php echo base_url() ?>index.php/chamados/cadastrar/<?php echo $id; ?>" class="mt-5">
    
    <div class="form-group">
        <label for="municipioChamado">Municipios</label>
        <select class="form-control" id="municipioChamado" name="municipioChamado">
            <option value=" <?php echo (isset($chamado)) ? $chamado[0]->mun_cli_id : ""  ?> "> <?php echo (isset($chamado)) ? $chamado[0]->mun_nome : "Selecione um cidade"  ?> </option>
           <?php foreach ($municipios as $key => $municipio) { ?>
            <option value="<?php echo $municipio->mun_cli_id  ?>" ><?php echo $municipio->mun_nome  ?></option>
          
           <?php  } ?>
        </select>
    </div>
    
    <div class="form-group">
        <label for="sistemaChamado">Sistema</label>
        <select class="form-control" id="sistemaChamado" name="sistemaChamado">
            <option value=" <?php echo (isset($chamado)) ? $chamado[0]->sis_id : ""  ?> "> <?php echo (isset($chamado)) ? $chamado[0]->sis_nome : "Selecione um sistema"  ?> </option>
           <?php foreach ($sistemas as $key => $sistema) { ?>
            <option value="<?php echo $sistema->sis_id  ?>" ><?php echo $sistema->sis_nome  ?></option>
          
           <?php  } ?>
        </select>
    </div>
    
    <div class="form-group">
        <label for="motivoChamado">Motivo</label>
        <input type="text" class="form-control"  id="motivoChamado" name="motivoChamado" value="<?php echo (isset($chamado)) ? $chamado[0]->cha_motivo : ""  ?>" placeholder="Motivo" />
    </div>
    <div class="form-group">
        <label for="descricaoChamado">Descrição</label>
        <input type="text" class="form-control"  id="descricaoChamado" name="descricaoChamado" value="<?php echo (isset($chamado)) ? $chamado[0]->cha_descricao : ""  ?>" placeholder="Descrição" />
    </div>
    <div class="form-group">
        <label for="autorChamado">Autor</label>
        <input type="text" class="form-control" id="autorChamado" name="autorChamado" placeholder="Autor" value="<?php echo (isset($chamado)) ? $chamado[0]->cha_autor : ""  ?>">
    </div>
    
    
    <div class="form-group">
        <label for="tipoContatoChamado">Tipo Contato</label>
        <select class="form-control" id="tipoContatoChamado" name="tipoContatoChamado">
            <option <?php echo (isset($chamado[0]->cha_tipo_contato) && $chamado[0]->cha_tipo_contato == 'email') ? "selected" : ""?> value="email">E-mail</option>
            <option <?php echo (isset($chamado[0]->cha_tipo_contato) && $chamado[0]->cha_tipo_contato == 'telefone') ? "selected" : ""?> value="telefone">Telefone</option>
            <option <?php echo (isset($chamado[0]->cha_tipo_contato) && $chamado[0]->cha_tipo_contato == 'whatsapp') ? "selected" : ""?> value="whatsapp">whatsapp</option>
          
        </select>
    </div>
    
    <div class="form-group">
        <label for="contatoChamado">Contato</label>
        <input type="text" class="form-control" id="contatoChamado" name="contatoChamado" placeholder="Contato" value="<?php echo (isset($chamado)) ? $chamado[0]->cha_contato : ""  ?>">
    </div>
    
    <?php  if($id != ""){ ?>
    
    <div class="form-group">
        <label  for="situacaoChamado">Solucionado</label>
        <select  class="form-control" id="situacaoChamado" name="situacaoChamado" onclick="situacao()">
            
            <option value="nao-solucionado">Não</option>
            <option value="solucionado">Sim</option>
          
        </select>
    </div>
    
    <div class="form-group solucao" style="display: none" >
        <label for="solucaoChamado">Solução do problema</label>
        <input type="text" class="form-control" id="solucaoChamado" name="solucaoChamado" placeholder="Solução do Problema" value="">
    </div>
    
    <?php } ?>
    <button type="submit" class="btn btn-primary">Salvar</button>

    
</form>


<script>
    
    function situacao(){
        
        var status = document.getElementById('situacaoChamado').value;
        console.log(status);
        if(status == "solucionado"){
            document.querySelector('.solucao').style.display = 'block';
           
        }else{
            document.querySelector('.solucao').style.display = 'none';
        }
        
        
    }



</script>