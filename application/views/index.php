<html>
    <head>
        <title>Index</title>
        <meta charset="utf-8">
            <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>bootstrap/css/bootstrap.min.css" type="text/css">
            <script src="<?php echo base_url('assets/'); ?>jquery.js"></script>
            
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
 
            <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
            
    </head>
    <body>
        
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">EstagioWM</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="<?php echo site_url('municipios'); ?>">Cidades <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo site_url('sistemas'); ?>">Sistemas</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo site_url('chamados'); ?>">Chamados</a>
                </li>
               
              </ul>
              
            </div>
          </nav>
        
        <div class="content container mt-5">
            <?php $this->load->view($pagina) ?>
        </div>
        
        
        <script src="<?php echo base_url('assets/'); ?>bootstrap/js/bootstrap.min.js"></script>
        
        
    </body>
    
    
</html>
