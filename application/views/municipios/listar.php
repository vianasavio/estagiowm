<?php ?>

<h1>Municipios cadastrado</h1>

<table class="table table-hover" id="tabelaListar">
    <thead>
        <tr>
            <th scope="col">Codigo</th>
            <th scope="col">Nome</th>
            <th scope="col">UF</th>
            <th scope="col">Data</th>
            <th scope="col">Opções</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($municipios as $key => $municipio) { ?>
            <tr>
                <th scope="row"><?php echo $municipio->mun_codigo; ?></th>
                <td><?php echo $municipio->mun_nome; ?></td>
                <td><?php echo $municipio->mun_uf; ?></td>
                <td><?php echo $municipio->mun_cli_data; ?></td>
                <td>
                    <button class="btn btn-success"  data-toggle="modal" data-target="#exampleModal" data-id="<?php echo $municipio->mun_cli_id; ?>" >Editar</button>
                    <a href="<?php echo site_url("municipios/deletar/$municipio->mun_cli_id") ?>" class="btn btn-danger">Deletar</a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>


<a href="./municipios/cadastrar" class="btn btn-primary">Novo Municipio</a>



<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Editar data de contrato</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form method="post" action="<?php echo base_url() ?>index.php/municipios/editar" class="mt-5">
                  
                    <div class="form-group">
                        <label for="dataContrato">Data contrato</label>
                        <input type="datetime-local" class="form-control" id="dataContrato" name="dataContrato" placeholder="Data Contrato">
                    </div>
                    
                    <div class="form-group">
                        <label for="motivoContrato">Motivo</label>
                        <input type="text" class="form-control" id="motivoContrato" name="motivoContrato" placeholder="Motivo do contrad">
                    </div>
                    
                    <input type="hidden" id="idMunicipio" name="idMunicipio">
              

      
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Salvar</button>
                
            </form>
        </div>
    </div>
</div>
    
    
    <script>
        $('#exampleModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var recipient = button.data('id'); // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this);
    
            modal.find('#idMunicipio').val(recipient);
          })
          
         $('#tabelaListar').DataTable({
        "pageLength": 5,
        "order": [[ 1, "asc" ]],      
        "language": {
            "lengthMenu": "Exibindo _MENU_ resultados por pagina",
            "zeroRecords": "Nenhum registro encontrado",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "Sem registros",
            "infoFiltered": "(Filtrado de _MAX_ resultados)",
            "loadingRecords": "Carregando...",
            "processing": "Processando...",
            "search": "Buscar:",
            "paginate": {
            "first": "Primeira",
            "last": "Ultima",
            "next": "<i class='fas fa-arrow-right'></i>",
            "previous": "<i class='fas fa-arrow-left'></i>"
            } 
        }        
    });
          
    </script>