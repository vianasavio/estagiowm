<?php

//var_dump($imp_municipio);
echo isset($imp_municipio[0]->mun_nome) ? $imp_municipio[0]->mun_nome : "" ;
?>

<h1>Cadastro de cidades</h1>

<form method="post" action="<?php echo base_url() ?>index.php/municipios/salvar" class="mt-5">
    <div class="form-group">
        <label for="nomeMunicipio">Cidade</label>
        <input type="text" class="form-control"  id="nomeMunicipio" name="nomeMunicipio" value="" />
        <a  class="btn btn-primary" onclick="buscarCidade()" href="#">Buscar</a>
    </div>
    <div class="form-group">
        <label for="codigoMunicipio">Codigo</label>
        <input type="text" class="form-control" id="codigoMunicipio" name="codigoMunicipio" placeholder="codigo" value="<?php echo isset($imp_municipio[0]->mun_nome) ? $imp_municipio[0]->mun_codigo : "" ; ?>">
    </div>
    
    <div class="form-group">
        <label for="ufMunicipio">UF</label>
        <input type="text" class="form-control" id="ufMunicipio" name="ufMunicipio" placeholder="UF" value="<?php echo isset($imp_municipio[0]->mun_uf) ? $imp_municipio[0]->mun_uf : "" ; ?>">
    </div>

    <div class="form-group">
        <label for="dataContrato">Data contrato</label>
        <input type="datetime-local" class="form-control" id="dataContrato" placeholder="Data Contrato">
    </div>
    <input type="hidden" id="idMunicipio" name="idMunicipio" value="<?php echo isset($imp_municipio[0]->mun_id) ? $imp_municipio[0]->mun_id : "" ; ?>"></input>
    <button type="submit" class="btn btn-primary">Salvar</button>

    
</form>

<script>

    function buscarCidade() {
    
        window.open('<?php echo site_url('municipios/pesquisar'); ?>', '_blank', 'width=1000,height=500,scrollbars=yes,menubar=no,status=yes,resizable=yes,screenx=0,screeny=0');
        return false;
        
    }
    
    function getDadosMunicipio(mun_id, mun_codigo, mun_nome, mun_uf) {
        
        document.getElementById('nomeMunicipio').value = mun_nome;
        document.getElementById('codigoMunicipio').value = mun_codigo;
        document.getElementById('ufMunicipio').value = mun_uf;
        document.getElementById('idMunicipio').value = mun_id;
        
        console.log(mun_nome);
        
    }

</script>