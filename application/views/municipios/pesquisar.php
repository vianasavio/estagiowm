<?php
    //print_r($imp_municipios);
?>


<html>

    <head>
        <title>Index</title>
        <meta charset="utf-8">
            <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>bootstrap/css/bootstrap.min.css" type="text/css">
    </head>    
    
    <body>
        
        <h1>Selecionar</h1>
        
        <div class="content container mt-5">
            <form method="GET" action="<?php echo base_url().'index.php/municipios/pesquisar/imp' ?>">
                <div class="form-row">
                  <div class="form-group col-md-5">
                    <label for="nomeMunicipio">Municipio</label>
                    <input type="text" class="form-control" id="nomeMunicipio" name="nomeMunicipio" placeholder="Municipio">
                  </div>
                  <div class="form-group col-md-5">
                    <label for="codigoMunicipio">codigo</label>
                    <input type="text" class="form-control" id="codigoMunicipio" name="codigoMunicipio" placeholder="Codigo">
                  </div>
                 
                  <div class="form-group col-md-2 mt-4">
                    <button type="submit" class="btn btn-primary">Pesquisar</button>
                  </div>

                </div>

             </form>

            <?php if(isset($imp_municipios)){ ?>
            <div>

                <table class="table table-hover">
                <legend>Municipios</legend>
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Codigo</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Opções</th>
                  </tr>
                </thead>
                <tbody>
                  
                  <?php foreach($imp_municipios as $imp_municipio){ ?>
                  <tr>
                    <th scope="row">1</th>
                    <td><?php echo $imp_municipio->mun_codigo ?></td>
                    <td><?php echo $imp_municipio->mun_nome ?></td>
                    <td><?php echo $imp_municipio->mun_uf ?></td>
                    <td><a onclick="setDados(<?php echo $imp_municipio->mun_id ?>, '<?php echo $imp_municipio->mun_codigo ?>', '<?php echo $imp_municipio->mun_nome ?>', '<?php echo $imp_municipio->mun_uf ?>' )" class="btn btn-success">Selecionar</a></td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>

             </div>
            <?php } ?>
        
        </div>    
        
        <script>
            
            function setDados(mun_id, mun_codigo, mun_nome, mun_uf){
                //alert(mun_nome);
                
                window.opener.getDadosMunicipio(mun_id, mun_codigo, mun_nome, mun_uf);
                
                window.close();
                //base_url(); ?>index.php/municipios/cadastrar/
            }
            
        </script>
        
    </body>
    
</html>
