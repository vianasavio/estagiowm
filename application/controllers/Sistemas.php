<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Sistemas extends CI_Controller {
    
    private $tbl = "index.php";
    private $dados = array();
    
    public function __construct() {
        parent::__construct();
        
       $this->load->model('sistemas_model');
        
    }
    
    
    public function index(){
        
        //$this->dados['municipios'] = $this->municipio_model->read(($_POST)?$_POST:null);
        // LEIA - Recuperando dados de pesquisa para exibição na tela.
        //$this->dados['search'] = ($_POST)?$_POST:null;
        // LEIA - Informando na variável página qual view deve ser carregada pela view principal (index.php)
        
        $this->dados['sistemas'] = $this->sistemas_model->getDados();
        
        $this->dados['pagina'] = 'sistemas/listar';
      
        $this->load->view($this->tbl, $this->dados);
        
    }
    
    
    public function cadastrar($id = ""){
            
        $this->dados['id'] = $id;
        
        
            if((!empty($this->input->post('nomeSistema')) && !empty($this->input->post('descricaoSistema')))){

                $values['sis_nome'] = $this->input->post('nomeSistema');
                $values['sis_descricao'] = $this->input->post('descricaoSistema');
                
                if($id != ""){
                     $this->dados['titulo'] = "Editar";
                     $this->sistemas_model->update($id, $values);
                    
                }else{
                    $this->dados['titulo'] = "Cadastrar";
                    $this->sistemas_model->insert($values);
                    
                }
                

                

                redirect('sistemas');

            }else{
                
                if(!empty($id)){
                    
                     $this->dados['titulo'] = "Editar";
                     $this->dados['sistema_unico'] = $this->sistemas_model->getDado($id);
                    
                    
                }else{
                   
                    $this->dados['titulo'] = "Cadastrar";
                    
                }
                
            }
   


            $this->dados['pagina'] = 'sistemas/cadastrar';

            $this->load->view($this->tbl, $this->dados);
        
    }//cadastra novo municipio
    
    public function  deletar($id){
        
        $this->sistemas_model->delete($id);
        
        redirect('sistemas');
        
    }
    
    
}
