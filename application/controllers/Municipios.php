<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Municipios extends CI_Controller {
    
    private $tbl = "index.php";
    private $dados = array();
    
    public function __construct() {
        parent::__construct();
        
       $this->load->model('municipios_model');
        
    }
    
    public function index(){
        
        //$this->dados['municipios'] = $this->municipio_model->read(($_POST)?$_POST:null);
        // LEIA - Recuperando dados de pesquisa para exibição na tela.
        //$this->dados['search'] = ($_POST)?$_POST:null;
        // LEIA - Informando na variável página qual view deve ser carregada pela view principal (index.php)
        
        $this->dados['municipios'] = $this->municipios_model->getDados();
        
        $this->dados['pagina'] = 'municipios/listar';
      
        $this->load->view($this->tbl, $this->dados);
        
    }
    


    
    
    public function cadastrar($get=""){
        $this->dados['imp_municipio']  = "";
        $this->dados['imp_municipio'] = $this->municipios_model->get_unique_imp($get);

        $this->dados['pagina'] = 'municipios/cadastrar';
       
        $this->load->view($this->tbl, $this->dados);
        
    }//cadastra novo municipio
    
  
    
    
    public function  pesquisar($get = ""){
        
        if($get == 'imp') {
            
            $this->dados['imp_municipios'] = $this->municipios_model->get_imp($this->input->get());

            
            $this->load->view('municipios/pesquisar', $this->dados);


            
        }else{
            
            $this->load->view('municipios/pesquisar', $this->dados);
            
        }
        
    }//pesquisar um municipio na tabela imp_municipio
    
    
    public function salvar() {
        
       if($this->input->post('idMunicipio')){
        
            $dados['mun_cli_data'] = $this->input->post('dataContrato');
            $dados['fk_mun_inc'] = $this->input->post('idMunicipio');

            $this->municipios_model->insert($dados);

            redirect('municipios');
            
       }


    }
    
    public function  editar(){
        
        $id = $this->input->post('idMunicipio');
        $data = $this->input->post('dataContrato');
        $motivo = $this->input->post('motivoContrato');
        
        //echo($this->input->post('dataContrato'));
        
        if(!empty($id) &&! empty($data) && !empty($motivo) ){
            
            $this->municipios_model->edit($id, $data, $motivo);
            
            //redirect('municipios');
            
        }
        
        redirect('municipios');
        
        
    }
    
    
    public function  deletar($id){
        
        $this->municipios_model->delete($id);
        
        redirect('municipios');
        
    }
    
    
}
