<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Chamados extends CI_Controller {
    
    private $tbl = "index.php";
    private $dados = array();
    
    public function __construct() {
        parent::__construct();
        
       $this->load->model('chamados_model');
       $this->load->library('session');
        
    }
    
    
    public function index(){
        
        
        
        $this->dados['chamados'] = $this->chamados_model->getChamados();
        
        $this->dados['pagina'] = 'chamados/listar';
      
        $this->load->view($this->tbl, $this->dados);
        
    }
    
    public function cadastrar($id = ""){
        
        $this->dados['id'] = $id;
        
        if($this->getIntervaloTempo($id) != 0){
            
            $this->session->set_flashdata('chamadosError', 'Tempo expirado para deletar ou alterar este chamado');
            redirect('chamados');
            
        }
        
        $this->load->model('municipios_model');
        $this->load->model('sistemas_model');
        $this->dados['municipios'] = $this->municipios_model->getDados();
        $this->dados['sistemas'] = $this->sistemas_model->getDados();
        
        if((!empty($this->input->post('municipioChamado')) && !empty($this->input->post('motivoChamado')) && !empty($this->input->post('descricaoChamado')) && 
                !empty($this->input->post('autorChamado')) && !empty($this->input->post('contatoChamado')) && !empty($this->input->post('sistemaChamado')) 
                && !empty($this->input->post('tipoContatoChamado')) )){

                $values['fk_municipios_cliente'] = $this->input->post('municipioChamado');
                $values['cha_motivo'] = $this->input->post('motivoChamado');
                $values['cha_descricao'] = $this->input->post('descricaoChamado');
                $values['cha_autor'] = $this->input->post('autorChamado');
                $values['cha_tipo_contato'] = $this->input->post('tipoContatoChamado');
                $values['cha_contato'] = $this->input->post('contatoChamado');
                $values['fk_sistema'] = $this->input->post('sistemaChamado');
                
                
                
                if($id !=""){
                    
                    if((!empty($this->input->post('situacaoChamado')))){
                        
                        $values['cha_situacao'] = $this->input->post('situacaoChamado');
                        $values['cha_solucao'] = $this->input->post('solucaoChamado');
                        $this->dados['titulo'] = "Editar";
                        $this->chamados_model->update($id, $values);
                    }
                     
                    
                }else{
                    $this->dados['titulo'] = "Cadastrar";
                    $this->chamados_model->insert($values);
                    
                }
                

                redirect('chamados');

            }else{
                
                if(!empty($id)){
                    
                     $this->dados['titulo'] = "Editar";
                     $this->dados['chamado'] = $this->chamados_model->getChamado($id);
                     
                    
                    
                }else{
                   
                    $this->dados['titulo'] = "Cadastrar";
                    
                }
                
            }
            
        
        
        $this->dados['pagina'] = 'chamados/cadastrar';

        $this->load->view($this->tbl, $this->dados);
        
    }//cadastra novo municipio
    
    

    public function  deletar($id){
           
        if($this->getIntervaloTempo($id) == 0){
            //$this->session->set_flashdata('', 'Tempo expirado para deletar ou alterar este chamado');
            $this->chamados_model->delete($id);    
        
        }else{
            
            $this->session->set_flashdata('chamadosError', 'Tempo expirado para deletar ou alterar este chamado');
        }
        
        
        redirect('chamados');
        
    }
    
    
    private function getIntervaloTempo($id){
        
        $chamado = $this->chamados_model->getChamado($id);
        $dataCadastro = $chamado[0]->cha_data;
        
        $data1 = new DateTime($dataCadastro);
        $data2 = new DateTime(date('Y-m-d\TH:i:s\Z'));
        
        $intervalo = $data1->diff($data2);
        
        
        return $intervalo->days;
    }
    
    
}//fim controller