<?php


class Chamados_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    
    
    public function insert($dados){
        
        return $this->db->set($dados)->insert('chamados');

    }//insere um sistema na tabela sistema
    
    
    public function update($id, $dados){

        $this->db->where('cha_id', $id);
        return $this->db->set($dados)->update('chamados');

    }

    public function delete($id){

        $this->db->where('cha_id', $id);
        return $this->db->set(array('cha_status' => 'inativo'))->update('chamados');

    }
    
    public function getChamado($id){
        
        $this->db->select('municipios_cliente.*, chamados.*, imp__municipios.mun_nome, sistemas.* ');    
 
        $this->db->join('municipios_cliente', 'chamados.fk_municipios_cliente = municipios_cliente.mun_cli_id');
        $this->db->join('imp__municipios', 'municipios_cliente.fk_mun_inc = imp__municipios.mun_id');
        $this->db->join('sistemas', 'chamados.fk_sistema = sistemas.sis_id');
        
        $this->db->where('chamados.cha_status', 'ativo');
        $this->db->where('chamados.cha_id', $id);

        return $this->db->get('chamados')->result();
        
    }
    
    public function getChamados(){
        
        $this->db->select('municipios_cliente.*, chamados.*, imp__municipios.mun_nome, sistemas.*');    
 
        $this->db->join('municipios_cliente', 'chamados.fk_municipios_cliente = municipios_cliente.mun_cli_id');
        $this->db->join('imp__municipios', 'municipios_cliente.fk_mun_inc = imp__municipios.mun_id');
        $this->db->join('sistemas', 'chamados.fk_sistema = sistemas.sis_id');
        
        $this->db->where('chamados.cha_status', 'ativo');

        return $this->db->get('chamados')->result();
        
    }
    
    
    
}//fim model