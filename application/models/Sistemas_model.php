<?php


class Sistemas_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    
    public function insert($dados){

        return $this->db->set($dados)->insert('sistemas');

    }//insere um sistema na tabela sistema
    
    public function delete($id){

        $this->db->where('sis_id', $id);
        return $this->db->set(array('sis_status' => 'inativo'))->update('sistemas');

    }
    
    public function update($id, $dados){

        $this->db->where('sis_id', $id);
        return $this->db->set($dados)->update('sistemas');

    }

    public function getDados(){
    
        $this->db->where('sis_status', 'ativo');
        return $this->db->get('sistemas')->result();
        
        
    }
    
    
    public function getDado($id){
        $this->db->where('sis_id', $id);
        $this->db->where('sis_status', 'ativo');
        return $this->db->get('sistemas')->result();
        
        
    }
    
    
    
}
