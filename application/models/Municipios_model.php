<?php


class Municipios_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    
    
    public function get_imp($get){

        if((isset($get['nomeMunicipio']) && !empty($get['nomeMunicipio'])) && (isset($get['codigoMunicipio']) && !empty($get['codigoMunicipio'])) ){

            $this->db->like('mun_nome', $get['nomeMunicipio']);
            $this->db->like('mun_codigo', $get['codigoMunicipio']);

        }elseif(isset($get['nomeMunicipio']) && !empty($get['nomeMunicipio'])){

            $this->db->like('mun_nome', $get['nomeMunicipio']);

        }elseif(isset($get['codigoMunicipio']) && !empty($get['codigoMunicipio'])){

            $this->db->like('mun_codigo', $get['codigoMunicipio']);
        }else{

            //$this->db->like('mun_codigo', 0);

        }

        return $this->db->get('imp__municipios')->result();
        
    }//busca os municipios da tabela imp_municipio
    
    
    public function get_unique_imp($get){

        $this->db->where('mun_id', $get);

        return $this->db->get('imp__municipios')->result();
        
    }//retorna um unico municipio imp
    
   
    public function insert($dados){

        return $this->db->set($dados)->insert('municipios_cliente');

    }//insere um cidade na tabela municipios clientes
    
    public function edit($id, $data, $motivo){

        $this->db->where('mun_cli_id', $id);
        return $this->db->set(array('mun_cli_data' => $data, 'mun_cli_motivo' => $motivo))->update('municipios_cliente');

    }//seta o status do municipio com inativo
    
    public function delete($id){

        $this->db->where('mun_cli_id', $id);
        return $this->db->set(array('status' => 'inativo'))->update('municipios_cliente');

    }//seta o status do municipio com inativo
    
    public function getDados(){
        
        $this->db->select('*');    
 
        $this->db->join('imp__municipios', 'municipios_cliente.fk_mun_inc = imp__municipios.mun_id');
        
         $this->db->where('status', 'ativo');

        return $this->db->get('municipios_cliente')->result();
        
    }
    
}
